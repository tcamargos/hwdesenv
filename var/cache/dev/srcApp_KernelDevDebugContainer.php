<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerOXrkEjX\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerOXrkEjX/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerOXrkEjX.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerOXrkEjX\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerOXrkEjX\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'OXrkEjX',
    'container.build_id' => 'ca073f89',
    'container.build_time' => 1572568400,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerOXrkEjX');
